package me.nidheesh.diagonalmachinetest.mvp;

import android.content.Context;

/**
 * Created by nidheesh on 27/09/2016.
 **/
interface MovieInteractor {


    void getMovies(Context context, int pageNum,MoviePresenter.GetList getList);
}