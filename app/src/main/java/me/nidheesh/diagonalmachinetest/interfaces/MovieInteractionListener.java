package me.nidheesh.diagonalmachinetest.interfaces;

/**
 * Created by nidheesh on 27/09/2016.
 **/

public interface MovieInteractionListener {

    void onLoadMore();

    void onItemClicked(int position);


}
